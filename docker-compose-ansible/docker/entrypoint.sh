#!/bin/sh

mkdir -p /root/.ssh
chmod 700 /root/.ssh
cp /ssh/id_rsa /root/.ssh
cp /ssh/id_rsa.pub /root/.ssh
chmod 600 /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa.pub

exec "$@"
