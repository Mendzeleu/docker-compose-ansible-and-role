Role Name backup_oracle_db_schema
=========

Role Variables
--------------
access_password - SYSTEM user password
user - user for backup
ora_dump_directory - directory object, directory for the dump (for example, DATA_PUMP_DIR by default)
dmp_directory - directory for dump



Example Playbook
----------------
#ansible-playbook -i hosts playbook.yml --extra-vars "access_password=your_SYSTEM_password"


---
- name: Backup ORACLE
  hosts: all
  remote_user: oracle
  become: true
  become_user: oracle

  vars:

  roles:
    - backup_oracle_db_schema



-------

BSD

Author Information
------------------

smendelev@tut.by
